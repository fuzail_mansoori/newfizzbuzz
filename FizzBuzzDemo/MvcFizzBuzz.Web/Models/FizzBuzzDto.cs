﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcFizzBuzz.Web.Models
{
    public class FizzBuzzDto
    {
        public int InputNumber { get; set; }
        public string Output { get; set; }
        public string color { get; set; }
    }
}