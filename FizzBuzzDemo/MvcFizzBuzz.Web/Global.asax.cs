﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject.Web.Common;
using Ninject;
using System.Reflection;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;

namespace MvcFizzBuzz.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : Ninject.Web.Common.NinjectHttpApplication
    {
     

       protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }



       protected override Ninject.IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());           
            kernel.Bind<IFuzzBuzzRepository>().To<FuzzBuzzRepository>();
            return kernel;
        }

    }


}





