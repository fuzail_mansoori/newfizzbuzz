﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace FizzBuzzTest
{
    [TestClass]
    public class FuzzBuzzRepositoryTest
    {

        private FuzzBuzzRepository fizzBuzzRepository;

        [TestInitialize]
        public void Setup()
        {
            fizzBuzzRepository = new FuzzBuzzRepository();
        }


        [TestMethod]
        public void Passing_number_Three_Returns_Three_Items_From_Repository()
        {
            const int expected = 3;
            var actual = fizzBuzzRepository.FindFizzBuzz(3);
            Assert.AreEqual(expected, actual.Count);

        }

        [TestMethod]
        public void Passing_number_Five_Returns_Five_Items_From_Repository()
        {
            const int expected = 5;
            var actual = fizzBuzzRepository.FindFizzBuzz(5);
            Assert.AreEqual(expected, actual.Count);
        }

        [TestMethod]
        public void Passing_number_Fifteen_Returns_Fifteen_Items_From_Repository()
        {
            const int expected = 15;
            var actual = fizzBuzzRepository.FindFizzBuzz(15);
            Assert.AreEqual(expected, actual.Count);
        }

        [TestMethod]
        public void Passing_number_Three_Returns_string_Fizz()
        {
           string expected = "Fizz";
            var actual = fizzBuzzRepository.GetFizz(3);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Passing_number_Five_Returns_string_Buzz()
        {
            string expected = "Buzz";
            var actual = fizzBuzzRepository.GetBuzz(5);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Passing_number_Fifteen_Returns_string_Fizz_Buzz()
        {
            string expected = "Fizz Buzz";
            var actual = fizzBuzzRepository.GetFizzBuzz(15);
            Assert.AreEqual(expected, actual);
        }
    }
}
