﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using Moq;
using MvcFizzBuzz.Web.Controllers;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;


namespace FizzBuzzTest
{
    [TestClass]
    public class FizzBuzzControllerTest
    {

        [TestMethod]
        public void Index_ActionResult_Returns_ViewResult()
        {
            var mockFuzzBuzzRepository = new Mock<IFuzzBuzzRepository>();

            // Arrange       

            var controller = new FizzBuzzController(mockFuzzBuzzRepository.Object);

            // Act
            var result = controller.Index(1) as ViewResult;

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }



        [TestMethod]
        public void Index_ActionResult_Returns_Index_View()
        {
            var mockFuzzBuzzRepository = new Mock<IFuzzBuzzRepository>();

            // Arrange     
            var controller = new FizzBuzzController(mockFuzzBuzzRepository.Object);

            // Act
            var result = controller.Index(1) as ViewResult;

            //Assert
            Assert.AreEqual("", result.ViewName);
        }

                    

        [TestMethod]
        public void Index_ActionResult_Returns_Lists_Of_Items()
        {
            var mockFuzzBuzzRepository = new Mock<IFuzzBuzzRepository>();

            List<FizzBuzzDto> mockFizzBuzz = new List<FizzBuzzDto>();
            mockFizzBuzz.Add(new FizzBuzzDto() { color = "Red", Output = "1" });
            mockFizzBuzz.Add(new FizzBuzzDto() { color = "Green", Output = "1" });
            mockFizzBuzz.Add(new FizzBuzzDto() { color = "Orange", Output = "1" });

            // Setup mock behavior for the GetAllFish() method in our repository           
            mockFuzzBuzzRepository.Setup(x => x.FindFizzBuzz(It.IsAny<int>())).Returns(mockFizzBuzz);  

            //Arrange           

            var controller = new FizzBuzzController(mockFuzzBuzzRepository.Object);

            FizzBuzzViewModel objmodel = new FizzBuzzViewModel();
            objmodel.InputNumber = 23;

            var result = controller.Index(objmodel) as ViewResult;
            var fizzBuzzList = (FizzBuzzViewModel)result.ViewData.Model;           

            //// Test the view model contains a list of fizzBuzz
            Assert.IsNotNull(fizzBuzzList, "The list of fizzBuzz does not exist");
            //Assert.AreEqual(3, viewDataModel.Count(), "The Acual count and expected count does not match");


        }



     


    }
}
