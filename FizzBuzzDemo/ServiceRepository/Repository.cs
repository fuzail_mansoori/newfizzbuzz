﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Models;

namespace ServiceRepository
{
  
   public class Repository<T> : IRepository<T> where T: BaseEntity
    {
       //protected readonly IFuzzBuzzRepository Repository;
       private IFizzBuzzServiceRepository ServiceRepository;

       public Repository(IFuzzBuzzRepository repository, IFizzBuzzServiceRepository serviceRepository)
        {
            //this.Repository = repository;
            this.ServiceRepository = serviceRepository;
        }


       public List<FizzBuzzDto> FizzBuzzList(int number)
       {
           var objFizzBuzzList = new List<FizzBuzzDto>();

           for (int count = 1; count <= number; count++)
           {
               FizzBuzzDto objFizzBuzz = new FizzBuzzDto();
               string result = ServiceRepository.GetFizzBuzz(count);

               if (result == "Fizz Buzz")
               {
                   objFizzBuzz.Output = result;
                   objFizzBuzz.color = "Red";
               }
               else if (result == "Buzz")
               {
                   objFizzBuzz.Output = result;
                   objFizzBuzz.color = "Green";
               }
               else if (result == "Fizz")
               {
                   objFizzBuzz.Output = result;
                   objFizzBuzz.color = "Blue";
               }
               else
               {
                   objFizzBuzz.Output = count.ToString();
               }

               objFizzBuzzList.Add(objFizzBuzz);
           }

           return objFizzBuzzList;
       }

      


        //public void Insert(T entity)
        //{
        //    try
        //    {
        //        if (entity == null)
        //        {
        //            throw new ArgumentNullException("entity");
        //        }
        //        this.Entities.Add(entity);
        //        this._context.SaveChanges();
        //    }
        //    catch (Exception exc)
        //    {
                
        //        throw;
        //    }
        //}

        
    }
}
