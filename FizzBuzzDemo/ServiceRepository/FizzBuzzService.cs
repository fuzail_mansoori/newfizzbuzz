﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Models;

namespace ServiceRepository
{
   public class FizzBuzzService :IFizzBuzzServiceRepository
   {
       public string GetFizzBuzz(int number)
       {
           string result = string.Empty;
           if (number % 3 == 0 && number % 5 == 0)                
               result = "Fizz Buzz";
           else
               result ="";

           return result;
       }
              

    }
}
