﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Models;

namespace ServiceRepository
{
   public class BuzzService
    {
       public string GetFizzBuzz(int number)
       {
           string result = null;

           if (number % 5 == 0)
               result = "Buzz";
           else
               result = "";

           return result;
       }

    }
}
