﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceRepository
{
   public interface IFizzBuzzServiceRepository
    {
       string GetFizzBuzz(int number);
    }
}
