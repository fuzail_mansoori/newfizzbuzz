﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcFizzBuzz.Web.Models;

namespace ServiceRepository
{
    public interface IFuzzBuzzRepository
    {
        List<FizzBuzzDto> FindFizzBuzz(int number);
        string GetFizz(int number);
        string GetBuzz(int number);
        string GetFizzBuzz(int number); 
    }
}
