﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcFizzBuzz.Web.Models;

namespace ServiceRepository
{
    public class FuzzBuzzRepository: IFuzzBuzzRepository
    {
      

   public List<FizzBuzzDto> FindFizzBuzz(int number)
        {            
            string result = string.Empty;
            var objFizzBuzzList = new List<FizzBuzzDto>();           
            for (int count = 1; count <= number; count++)
            {
                FizzBuzzDto objFizzBuzz = new FizzBuzzDto();

                        if (count % 3 == 0 && count % 5 == 0)
                        {
                            result = GetFizzBuzz(count);
                            result = ReplaceFizzBuzzChar(result);
                            objFizzBuzz.Output = result;
                            objFizzBuzz.color = "Red";                           
                        }
                        else if (count % 5 == 0)
                        {
                            result = GetBuzz(count);
                            result = ReplaceFizzBuzzChar(result);
                            objFizzBuzz.Output = result;
                            objFizzBuzz.color = "Green";                           
                        }
                        else if (count % 3 == 0)
                        {
                            result = GetFizz(count);
                            result = ReplaceFizzBuzzChar(result);
                            objFizzBuzz.Output = result;
                            objFizzBuzz.color = "Blue";                           
                        }
                        else
                        {
                            objFizzBuzz.Output = count.ToString();
                           
                        }

                        objFizzBuzzList.Add(objFizzBuzz);
                  }

            return objFizzBuzzList;                  

        }
      

   public string GetFizz(int number)
     {
            string result = null;

            if (number % 3 == 0)
                result = "Fizz";
            else
                result = "";

            return result;

      }


   public string GetBuzz(int number)
   {
       string result = null;

       if (number % 5 == 0)
           result = "Buzz";
       else
           result = "";

       return result;
   }


   public string GetFizzBuzz(int number)
   {
       string result = string.Empty;
       if (number % 3 == 0 && number % 5 == 0)
           result = "Fizz Buzz";
       else
           result = "";

       return result;
   }



   public string ReplaceFizzBuzzChar(string fizzBuzz)
       {
           string charDdayOfWeek = DateTime.Now.DayOfWeek.ToString().Substring(0, 1);
           string result = string.Empty;
          
           if (fizzBuzz.Contains("Fizz Buzz"))
           {
               result = fizzBuzz.Replace("F", charDdayOfWeek);
               result = result.Replace("B", charDdayOfWeek); 
           }
           else if (fizzBuzz.Contains("Fizz"))
           {
              result = fizzBuzz.Replace("F", charDdayOfWeek);              
           }
           else if (fizzBuzz.Contains("Buzz"))
           {
               result = fizzBuzz.Replace("B", charDdayOfWeek);         
           }           
           else
           {
               result = fizzBuzz;
           }
           return result;

       }


        

     }
 }

