﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Models;

namespace ServiceRepository
{
    public class FizzService : IFizzBuzzServiceRepository
    {
          public string GetFizzBuzz(int number)
           {
                   string result = null;

                   if (number % 3 == 0)                  
                       result = "Fizz";
                   else
                       result = "";                 

                   return result;
            }
                           
       }

    }

