﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;

namespace FizzBuzzTest
{
    [TestClass]
    public class UnitOfWorkTest
    {

        private UnitOfWork unitOfWork;

        [TestInitialize]
        public void Setup()
        {
            unitOfWork = new UnitOfWork();
        }

        [TestMethod]
        public void Passing_Number_Fifteen_Returns_Output_Items_Count_Fifteen()
        {
            int expected = 15;
            var actual = unitOfWork.GetAllFizzBuzz(15);
            Assert.AreEqual(expected, actual.Count);
        }

    }
}
