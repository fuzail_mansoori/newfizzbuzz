﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;

namespace FizzBuzzTest
{
    [TestClass]
    public class FuzzBuzzRepositoryTest
    {

        private FizzBuzzRepository fizzBuzzRepository;

        [TestInitialize]
        public void Setup()
        {
            fizzBuzzRepository = new FizzBuzzRepository();
        }

        [TestMethod]
        public void Passing_Number_Fifteen_Returns_Output_Boolean_True()
        {
            bool expected = true;
            var actual = fizzBuzzRepository.Calculate(15);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Methods_Returns_Output_String_FizzBuzz()
        {
            string expected = "Fizz Buzz";
            var actual = fizzBuzzRepository.GetMessage();
            Assert.AreEqual(expected, actual);
        }

    }
}
