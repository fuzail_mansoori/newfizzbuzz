﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcFizzBuzz.Web.Models;
using MvcFizzBuzz.Web.Repository;

namespace FizzBuzzTest
{
    [TestClass]
    public class FizzRepositoryTest
    {
        private FizzRepository fizzRepository;

        [TestInitialize]
        public void Setup()
        {
            fizzRepository = new FizzRepository();
        }


        [TestMethod]
        public void Passing_number_Three_Returns_Output_Boolean_True()
        {
            bool expected = true;
            var actual = fizzRepository.Calculate(15);
            Assert.AreEqual(expected, actual);
        }
               

        [TestMethod]
        public void Methods_Returns_Output_String_FizzBuzz()
        {
            string expected = "Fizz";
            var actual = fizzRepository.GetMessage();
            Assert.AreEqual(expected, actual);
        }



    }
}
