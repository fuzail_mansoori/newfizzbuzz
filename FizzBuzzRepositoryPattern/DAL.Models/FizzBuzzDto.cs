﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
   public class FizzBuzzDto
    {        
        public int InputNumber { get; set; }       
        public string Output { get; set; }       
        public string color { get; set; }
        
    }
}
