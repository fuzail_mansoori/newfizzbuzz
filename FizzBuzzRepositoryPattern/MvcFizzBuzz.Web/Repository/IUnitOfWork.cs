﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcFizzBuzz.Web.Models;

namespace MvcFizzBuzz.Web.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        List<FizzBuzzDto> GetAllFizzBuzz(int number);
    }
    
}
