﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvcFizzBuzz.Web.Models;

namespace MvcFizzBuzz.Web.Repository
{
    public interface IFizzBuzzRepository
    {       
        bool Calculate(int number);
        string GetMessage();
    }
}