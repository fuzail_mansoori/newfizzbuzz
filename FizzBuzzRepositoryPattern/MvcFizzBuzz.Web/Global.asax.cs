﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject.Web.Common;
using Ninject;
using System.Reflection;
using MvcFizzBuzz.Web.Models;
using System.Linq.Expressions;
using MvcFizzBuzz.Web.Repository;


namespace MvcFizzBuzz.Web
{
   
    public class MvcApplication : Ninject.Web.Common.NinjectHttpApplication
    {
     
       protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }



       protected override Ninject.IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();       
            return kernel;

        }

    }


}





